/*
  *Nombre archivo: scene.h
  *Autor: Cristian Leonardo Ríos López
  *Fecha de creación: Septiembre 2011
  *Fecha de ultima modificación: Enero  2012
  *Descripción: la clase scene permite elaborar el dibujo del tablero y los carros, también permite realizar la animacion de dichos
  *             elementos, mostrando los diferentes estados del tablero para ir desde el tablero inicial al tablero que tiene al
  *             carro A en la meta.
  *Universidad del Valle
*/

#ifndef SCENE_H
#define SCENE_H

#include <QGraphicsScene>
#include <QSequentialAnimationGroup>
#include <QString>
#include "item.h"
#include "chip.h"
#include "action.h"

class Scene : public QGraphicsScene
{
    Q_OBJECT

public:

    Scene(int rowsMax, int columnsMax, QObject *parent = 0);
    ~Scene();

    void clearBoard();
    void drawGrid();
    char** getBoard();
    int getColumnMax();
    int getRowMax();
    void newGame(char** board,QVector<Chip> *chips);
    void setColumnMax(int columnsMany);
    void setRowMax(int rowsMany);
    void runAnimation(QVector<Action> moves);

private:
    QSequentialAnimationGroup *animationGroup;
    char** board;
    Chip *chipGame;
    QVector<Chip> *chips;
    QVector<Item*> chipsGraphics;
    int columnMax;
    int freeBlack;
    int freeWhite;
    bool gameHuman;
    bool isCheckEndMin;
    bool isCheckEndMax;
    bool isCheckMin;
    bool isDrawGame;
    int rowMax;
    QVector<QPoint> possibleMoves;
    QGraphicsEllipseItem *selected;
    QVector<QGraphicsRectItem*> selectPossibleMoves;

    void clearPossibleMoves();
    void drawBoard();
    void drawElements();
    void drawSelectChip(QPoint position);
    void drawSelectPossibleMove();
    void eraseSelectPossibleMove();
    Item* findChip(char id);
    Chip* findChip(QPoint position, char id='z');
    QPoint findFreePosition(char id);
    QPoint howPositionBoard(QPointF position);
    void mensaje(QString msj);
    void moveIsAttack(QPoint position, QVector<Action> *moves);
    bool moveValid(QPoint position);
    void printBoard();
    void setBoard(char** board);
    void setChips(QVector<Chip> *chips);
    void updateBoard(QPoint positionBefore, QPoint positionAfter, char letter);
    bool validatePosition(QPoint position);
    QString whichImage(Chip *chip);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);

signals:
    void moveMade();
    void getPossibleMoves(char);
    void verifyInCheckEndMin();
    void verifyInCheckMin(Action);
    void verifyDrawGame();

private slots:
    void animationFinished();

public slots:
    void moveMadeResponse(Action);
    void setPossibleMoves(QVector<QPoint>);
    void inCheckEndMin(bool);
    void inCheckMin(bool);
    void drawGame(bool);

};

#endif // SCENE_H
