#include "game.h"
#include <iostream>
#include <ctime>
#include <QDebug>
#include <cmath>

using namespace std;

Game::Game(int columnMax,int rowMax, QObject *parent)
    :QObject(parent)
{
    this->columnMax= columnMax;
    this->rowMax= rowMax;
    board= new char*[this->rowMax];
    for(int i=0; i<this->rowMax; i++)
    {
        board[i]= new char[this->columnMax];
    }
    for(int i=0;i<this->rowMax;i++)
    {
        for(int j=0;j<this->columnMax;j++)
        {
            board[i][j] = '0';
        }
    }
    amountPawns = 4;
    amountKnights = 1;
    amountRooks = 0;
    amountQueens = 1;
    amountKings = 1;
    amountBishops = 1;

    depth = 2;

    qsrand(time(NULL));
}

Game::~Game()
{
    for(int i=0; i<this->rowMax; i++)
    {
        delete this->board[i];
        this->board[i] = 0;
    }

    delete this->board;
    this->board = 0;
}

Node* Game::applyAction(Node* dad, Action* action)
{
    char **boardNew = dad->getBoard();
    QVector<Chip> chipsNew = dad->getChips();
    int depthNew = dad->getDepth()+1;
    Node::type type;
    char id = action->getId();
    QPoint positionNew = action->getPosition();
    Chip *chip = findChip(id,&chipsNew);

    if(depthNew%2 == 0)
    {
        type = Node::Max;
    }else
    {
        type = Node::Min;
    }

    QPoint positionBefore = chip->getPosition();
    //se actualiza la poscicion de la ficha con la qu se jugó
    chip->setPosition(positionNew);

    //si la jugada ralizada come una pieza se se quita esta del vector
    if(boardNew[positionNew.x()][positionNew.y()] != '0')
    {
        int index;
        //se busca la pieza que fue comida
        for(int i=0;i<chipsNew.size();i++)
        {
            if((chipsNew[i].getPosition() == positionNew) && chipsNew[i].getId() != id)
            {
                index = i;
                break;
            }
        }

        //se quita la pieza que fuecomida
        chipsNew.remove(index);
    }

    //se actualiza el nuevo tablero
    updatedBoard(boardNew,positionBefore,positionNew,id);

    Node *node = new Node(dad,boardNew,columnMax,rowMax,chipsNew,type,depthNew,*action);

    /*-------------------------------*/
    chip = 0;
    /*--------------------------------*/
    //cout  << "termina applyAction" << endl;

    return node;
}

void Game::clearStates()
{
    for(int i=0;i<this->rowMax;i++)
    {
        for(int j=0;j<this->columnMax;j++)
        {
            board[i][j] = '0';
        }
    }
    chips.clear();
}

void Game::createChips(int amount,Chip::type type,QChar id,QVector<QPoint>* positions)
{
    for(int i=0,count=0;i<amount;i++)
    {
        int index = qrand()%positions->size();
        QPoint position = positions->at(index);
        Chip a;
        if(type == Chip::Peon)
        {
            a = Chip(position,Chip::Blanco,type,id.toAscii()+count);
            board[position.x()][position.y()] = id.toAscii()+count;
            count++;
        }else
        {
            a = Chip(position,Chip::Blanco,type,id.toAscii());
            board[position.x()][position.y()] = id.toAscii();
        }
        positions->remove(index);
        chips.append(a);

        index = qrand()%positions->size();
        position = positions->at(index);
        Chip b;
        if(type == Chip::Peon)
        {
            b = Chip(position,Chip::Negro,type,id.toAscii()+count);
            board[position.x()][position.y()] = id.toAscii()+count;
            count++;
        }
        else
        {
            b = Chip(position,Chip::Negro,type,id.toUpper().toAscii());
            board[position.x()][position.y()] = (id.toUpper()).toAscii();
        }
        positions->remove(index);
        chips.append(b);
    }

    //cout  << "termina createChips" << endl;
}

int Game::distanceToKing(Chip::color color, QPoint position,QVector<Chip> chips)
{

    int x = position.x();
    int y = position.y();

    int distanceTotal = 0;

    for(int i=0;i<chips.size();i++)
    {
        Chip chip = chips.at(i);

        QPoint point = chip.getPosition();
        int xActual = point.x();
        int yActual = point.y();

        if(chip.getColor() != color)
        {                       
            distanceTotal += fabs(x-xActual)+ fabs(y-yActual);

        }
    }

    //cout  << "termina distanceToKing" << endl;

    return distanceTotal;
}

QVector<Action> Game::findActions(Chip *chip, char **boardFind)
{
    QVector<Action> actions;

    Chip::color color= chip->getColor();

    //movimientos peon
    int dyPeon[]= {-1,1,0};
    int dxPeonBlanco[]= {1,1,1};
    int dxPeonNegro[]= {-1,-1,-1};

    //movimientos caballos
    int dyCaballo[] = {2,1,-1,-2,-2,-1,1,2};
    int dxCaballo[] = {-1,-2,-2,-1,1,2,2,1};

    //movimiento alfil
    int dyAlfil[] = {1,1,-1,-1};
    int dxAlfil[] = {1,-1,1,-1};
    bool obstacleAlfil[]={false,false,false,false};

    //movimiento reina
    int dyReina[] = {1,1,-1,-1,1,0,-1,0};
    int dxReina[] = {1,-1,1,-1,0,1,0,-1};
    bool obstacleReina[]={false,false,false,false,false,false,false,false};

    int xActual =chip->getPosition().x();
    int yActual =chip->getPosition().y();

    int xNew, yNew;

    switch(chip->getType())
    {

    case Chip::Peon:

        for(int i=0; i<3; i++)
        {
            yNew=yActual+dyPeon[i];
            if(color==Chip::Blanco)
                xNew=xActual+dxPeonBlanco[i];
            else
                xNew=xActual+dxPeonNegro[i];

            if(positionValidate(xNew, yNew))
            {
                if(i!=2 && boardFind[xNew][yNew]!='0' &&
                        isAttack(boardFind[xNew][yNew], color))
                {
                    Action action(chip->getId(), QPoint(xNew, yNew));
                    actions.append(action);
                }else if(i==2 && boardFind[xNew][yNew]=='0')
                {
                    Action action(chip->getId(), QPoint(xNew, yNew));
                    actions.append(action);
                }
            }
        }
        break;
    case Chip::Alfil:
        for(int i=1;i<rowMax;i++)
        {
            for(int j=0;j<4;j++)
            {
                xNew = xActual+i*dxAlfil[j];
                yNew = yActual+i*dyAlfil[j];

                if(positionValidate(xNew,yNew) && !obstacleAlfil[j])
                {
                    bool attack= isAttack(boardFind[xNew][yNew], color);
                    if(attack)
                    {
                        Action action(chip->getId(), QPoint(xNew, yNew));
                        actions.append(action);
                        obstacleAlfil[j]=true;
                    }else if(board[xNew][yNew]=='0')
                    {
                        Action action(chip->getId(), QPoint(xNew, yNew));
                        actions.append(action);

                    }else
                    {
                        obstacleAlfil[j]=true;
                    }
                }
            }
        }
        break;
    case Chip::Caballo:
        for(int i=0; i<8; i++)
        {
            xNew=xActual+dxCaballo[i];
            yNew=yActual+dyCaballo[i];

            if(positionValidate(xNew, yNew))
            {
                if(isAttack(boardFind[xNew][yNew],color) || boardFind[xNew][yNew]=='0')
                {
                    Action action(chip->getId(), QPoint(xNew, yNew));
                    actions.append(action);
                }
            }
        }
        break;
    case Chip::Reina:
        for(int i=1;i<rowMax;i++)
        {
            for(int j=0;j<8;j++)
            {
                xNew = xActual+i*dxReina[j];
                yNew = yActual+i*dyReina[j];

                if(positionValidate(xNew,yNew) && !obstacleReina[j])
                {
                    if(isAttack(boardFind[xNew][yNew],color))
                    {
                        Action action(chip->getId(), QPoint(xNew, yNew));
                        actions.append(action);
                        obstacleReina[j] = true;
                    }else if(boardFind[xNew][yNew] == '0')
                    {
                        Action action(chip->getId(), QPoint(xNew, yNew));
                        actions.append(action);
                    }else
                    {
                        obstacleReina[j] = true;
                    }
                }
            }
        }
        break;
    case Chip::Rey:
        for(int j=0;j<8;j++)
        {
            xNew = xActual+1*dxReina[j];
            yNew = yActual+1*dyReina[j];

            if(positionValidate(xNew,yNew))
            {
                if(isAttack(boardFind[xNew][yNew],color)||boardFind[xNew][yNew] == '0' )
                {
                    Action action(chip->getId(), QPoint(xNew, yNew));
                    actions.append(action);
                }
            }
        }
        break;
    }
    /*--------------------------------*/
    chip = 0;
    /*--------------------------------*/

    //cout  << "termina findActions" << endl;
    return actions;
}

Chip* Game::findChip(char id,QVector<Chip> *chipsFind)
{
    Chip *chip = 0;
    if(chipsFind == 0)
    {
        for(int i=0;i<chips.size();i++)
        {
            if(chips[i].getId() == id)
            {
                chip = &chips[i];
                break;
            }
        }
    }else
    {
        for(int i=0;i<chipsFind->size();i++)
        {
            if((*chipsFind)[i].getId() == id)
            {
                chip = &(*chipsFind)[i];
                break;
            }
        }
    }
    //cout  << "termina findChip" << endl;

    return chip;
}

void Game::findKingPosition(QVector<Chip> chips, QPoint* whiteKingPosition, QPoint* blackKingPosition)
{
    for(int i=0;i<chips.size();i++)
    {
        Chip chip = chips.at(i);

        if(chip.getColor() == Chip::Blanco)
        {
            if(chip.getId() == 'k')
            {
                whiteKingPosition->setX(chip.getPosition().x());
                whiteKingPosition->setY(chip.getPosition().y());
            }

        }else if(chip.getColor() == Chip::Negro)
        {
            if(chip.getId() == 'K')
            {
                blackKingPosition->setX(chip.getPosition().x());
                blackKingPosition->setY(chip.getPosition().y());
            }
        }
    }

    //cout  << "termina findKingPosition" << endl;
}

//Retorna el valor ponderado de las fichas que se ha comido el equipo de color->color.
int Game::findValueChips(Chip::color color, QVector<Chip> chips)
{
    int valueP = 2;
    int valueC = 10;
    int valueA = 10;
    int valueQ = 20;



    int valueTotal = 0 ;
    //No se tiene encuenta el rey porque ese no se puede comer.


    for(int i=0;i<chips.size();i++)
    {
        Chip chip = chips.at(i);

        if(chip.getColor() == color)
        {
            if(chip.getType() == Chip::Peon)
            {
                valueTotal += valueP;
            }else if(chip.getType() == Chip::Caballo)
            {
                valueTotal += valueC;
            }else if(chip.getType() == Chip::Alfil)
            {
                valueTotal += valueA;
            }else if(chip.getType() == Chip::Reina)
            {
                valueTotal += valueQ;
            }
        }
    }

    //cout  << "termina findValueChips" << endl;

    return valueTotal;
}

void Game::generateBoard()
{
    QVector<QPoint> *positions = new QVector<QPoint>;

    bool boardComplete = false;

    while(!boardComplete)
    {
        for(int i=0;i<rowMax;i++)
        {
            for(int j=0;j<columnMax;j++)
            {
                positions->append(QPoint(i,j));
            }
        }
        //cout << "genero posicioens" << endl;
        createChips(amountPawns,Chip::Peon,'1',positions);
        //cout << "genero peones" << endl;
        createChips(amountKnights,Chip::Caballo,'c',positions);
        createChips(amountQueens,Chip::Reina,'q',positions);
        createChips(amountBishops,Chip::Alfil,'a',positions);
        //        createChips(amountKings,Chip::Rey,'k',positions);
        //createChips(amountRooks,Chip::Torre,id,positions);

        bool kingsFull = false;

        bool blackComplete = false;
        bool whiteComplete = false;

        while(!kingsFull && positions->size() > 0)
        {
            int index = qrand()%positions->size();
            //cout << "aleatorio " << index << endl;
            QPoint position = positions->at(index);

            bool noCheck = false;

            if(!whiteComplete)
            {
                noCheck = !inCheck(position,Chip::Blanco);
                if(noCheck)
                {
                    board[position.x()][position.y()] = 'k';
                    Chip a(position,Chip::Blanco,Chip::Rey,'k');
                    chips.append(a);

                    positions->remove(index);

                    //Posicion para otro rey black
                    index = qrand()%positions->size();
                    //cout << "aleatorio " << index << endl;
                    position = positions->at(index);
                    whiteComplete = true;
                }
            }

            if(!blackComplete)
            {
                noCheck = !inCheck(position,Chip::Negro);
                if(noCheck)
                {
                    board[position.x()][position.y()] = 'K';
                    Chip a(position,Chip::Negro,Chip::Rey,'K');
                    chips.append(a);
                    positions->remove(index);
                    blackComplete = true;
                }
            }

            if(blackComplete && whiteComplete)
            {
                kingsFull = true;
                boardComplete=true;

            }else if(!blackComplete && !whiteComplete)
            {
                positions->remove(index);
            }

            //cout << noCheck <<endl;
        }

        if(!boardComplete)
        {
            this->clearStates();
            cout << "No se completo" << endl;
        }
    }

    /*--------------------------------*/
    positions->clear();
    delete positions;
    positions = 0;
    /*--------------------------------*/

    //cout  << "termina generateBoard" << endl;
}

char** Game::getBoard()
{
    return board;
}

char** Game::getCopyBoard()
{
    char **boardCopy = new char*[this->rowMax];
    for(int i=0; i<this->rowMax; i++)
    {
        boardCopy[i]= new char[this->columnMax];
    }
    for(int i=0;i<this->rowMax;i++)
    {
        for(int j=0;j<this->columnMax;j++)
        {
            boardCopy[i][j] = board[i][j];
        }
    }

    return boardCopy;
}

QVector<Chip>* Game::getChips()
{
    return &chips;
}

int Game::getColumnMax()
{
    return columnMax;
}

void Game::getPossibleMoves(char id)
{
    Chip *chip = findChip(id);
    QVector<Action> possibleActions = findActions(chip,this->board);
    QVector<QPoint> possibleMovesFind;

    foreach(Action a,possibleActions)
    {
        possibleMovesFind.append(a.getPosition());
    }

    //mostrar los movimientos posibles encotradas
    /*foreach(QPoint i,possibleMoves)
    {
        qDebug() << "position: (" << i.x() << "," << i.y() << ")";
    }*/

    /*--------------------------------*/
    chip = 0;
    /*--------------------------------*/

    //cout  << "termina getPosibleMoves" << endl;

    emit possibleMoves(possibleMovesFind);
}

int Game::getRowMax()
{
    return rowMax;
}

int Game::getDepth()
{
    return depth;
}

bool Game::inCheck(QPoint possiblePosition, Chip::color color,QVector<Chip> *chipsFind, bool heuristic, char **boardFind)
{
    if(chipsFind == 0)
    {
        chipsFind = &chips;
    }
    if(boardFind == 0)
    {
        boardFind = this->board;
    }

    bool inCheck = false;

    if(!heuristic)
    {
        if(color == Chip::Blanco)
        {
            boardFind[possiblePosition.x()][possiblePosition.y()] = 'k';
        }else
        {
            boardFind[possiblePosition.x()][possiblePosition.y()] = 'K';
        }
    }

    QVector<Action> actions;

    for(int i=0;i<chipsFind->size() && !inCheck;i++)
    {
        Chip c = chipsFind->at(i);

        if(color != c.getColor())
        {
            actions  = findActions(&c,boardFind);
            //qDebug()<<"id c: " <<c->getId();
            /*foreach(Action a,actions){
               qDebug()<< a.toString();
           }*/

        }

        for(int j=0; j<actions.size();j++)
        {
            Action action = actions.at(j);
            QPoint point = action.getPosition();

            if(point == possiblePosition)
            {
                inCheck = true;
                break;
            }
        }
        actions.clear();
    }

    if(!heuristic)
    {
        boardFind[possiblePosition.x()][possiblePosition.y()] = '0';
    }
    //cout  << "termina inCheck" << endl;

    return inCheck;
}

bool Game::inCheckEndMin()
{
    Action null;
    Node *rootNode = new Node(0,this->getCopyBoard(),columnMax,rowMax,chips,Node::Min,0,null);

    bool check = this->inCheckEnd(rootNode);

    delete rootNode;
    rootNode = 0;

    emit inCheckEndMinResponse(check);
    return check;
}

bool Game::inCheckEnd(Node* node)
{
    bool check = true;

    QVector<Chip> chipsFind = node->getChips();
    char **boardFind = node->getBoard();
    Node::type type = node->getType();

    if(type == Node::Max)
    {
        for(int i=0;i<chipsFind.size() && check;i++)
        {
            Chip c = chipsFind.at(i);

            if(c.getColor() == Chip::Blanco)
            {
                QVector<Action> actions = findActions(&c,boardFind);

                foreach(Action a,actions)
                {
                    Node *expandedNode = applyAction(node,&a);
                    QVector<Chip> c = expandedNode->getChips();
                    QPoint kingPosition = findChip('k',&c)->getPosition();
                    check = inCheck(kingPosition, Chip::Blanco, &c, true, expandedNode->getBoard());
                    delete expandedNode;
                    expandedNode = 0;
                    if(!check)
                        break;
                }
            }
        }
    }else if(type == Node::Min)
    {
        for(int i=0;i<chipsFind.size() && check;i++)
        {
            Chip c = chipsFind.at(i);

            if(c.getColor() == Chip::Negro)
            {
                QVector<Action> actions = findActions(&c,boardFind);

                foreach(Action a,actions)
                {
                    Node *expandedNode = applyAction(node,&a);
                    QVector<Chip> c = expandedNode->getChips();
                    QPoint kingPosition = findChip('K',&c)->getPosition();
                    check = inCheck(kingPosition, Chip::Negro, &c, true, expandedNode->getBoard());
                    delete expandedNode;
                    expandedNode = 0;
                    if(!check)
                        break;
                }
            }
        }
    }

    return check;
}

//slot
bool Game::inCheckMin(Action action)
{
    Action null;
    Node *rootNode = new Node(0,this->getCopyBoard(),columnMax,rowMax,chips,Node::Min,0,null);

    bool check = true;

    Node *expandedNode = applyAction(rootNode,&action);
    QVector<Chip> c = expandedNode->getChips();
    QPoint kingPosition = findChip('K',&c)->getPosition();
    check = inCheck(kingPosition, Chip::Negro, &c, true, expandedNode->getBoard());
    delete expandedNode;
    expandedNode = 0;

    delete rootNode;
    rootNode = 0;

    emit inCheckMinResponse(check);
    return check;
}

//Retorna true si es ataque
bool Game::isAttack(char id, Chip::color color)
{
    bool attack=false;
    QChar qId(id);

    if(color==Chip::Blanco)
    {
        if(qId.isNumber() && qId.toAscii()%2==0 && qId.toAscii()!='0')
            attack =true;
        else if(qId.isUpper())
            attack =true;

    }else {
        if(qId.isNumber() && qId.toAscii()%2!=0)
            attack =true;
        else if(qId.isLower())
            attack =true;
    }

    //cout  << "termina isAttack" << endl;

    return attack;
}

void Game::play()
{
    //comprueba si está en empate, si asi es ya no realiza ninguna jugada
    if(this->drawGame())
        return;

    int cantidad = 0;
    QVector<Node*> tree;

    Action null;
    Node *rootNode = new Node(0,this->getCopyBoard(),columnMax,rowMax,chips,Node::Max,0,null);
    tree.append(rootNode);

    for(int i=0;i<tree.size();i++)
    {
        Node *node = tree[i];
        if(node->getDepth() == depth)
            break;

        QVector<Chip> chipsNode = node->getChips();
        char **boardFind = node->getBoard();
        QPoint whiteKingPosition(-1,-1);
        QPoint blackKingPosition(-1,-1);

        foreach(Chip c,chipsNode)
        {
            QVector<Action> actions;

            if(node->getType() == Node::Max)
            {
                if(c.getColor() == Chip::Blanco)
                {
                    actions = findActions(&c,boardFind);

                    if(actions.isEmpty())
                    {
                        node->setIsLeft(true);
                    }

                    foreach(Action a,actions)
                    {
                        Node *b = applyAction(node,&a);
                        QVector<Chip> chips = b->getChips();
                        findKingPosition(chips,&whiteKingPosition,&blackKingPosition);

                        bool check = inCheck(whiteKingPosition,Chip::Blanco,&chips,true,b->getBoard());

                        if(!check)
                        {
                            tree.append(b);
                        }
                    }
                }
            }else{
                if(c.getColor() == Chip::Negro)
                {
                    actions = findActions(&c,boardFind);

                    if(actions.isEmpty())
                    {
                        node->setIsLeft(true);
                    }

                    foreach(Action a,actions)
                    {
                        Node *b = applyAction(node,&a);
                        QVector<Chip> chips = b->getChips();
                        findKingPosition(chips,&whiteKingPosition,&blackKingPosition);

                        bool check = inCheck(blackKingPosition,Chip::Negro,&chips,true,b->getBoard());

                        if(!check)
                        {
                            tree.append(b);
                        }
                    }
                }
            }
        }
    }

    //se prueba si max quedo en jaquemate
    if(tree.size()==1)
    {
        delete rootNode;
        rootNode = 0;
        tree.clear();
        Action move('J',QPoint());
        emit moveResponse(move);
        return;
    }

    //aplicar heuristica hasta que cambie de profundidad o si es nodo hoja
    //actualizar las utilidades
    while(tree.size() != 1)
    {
        Node *node = tree.last();
        if((node->getDepth() == depth) || node->getIsLeft())
        {
            int utility = utilityHeuristicFunction(node);
            node->setUtility(utility);
            node->updateUtilityDad();
            tree.remove(tree.size()-1);
            cantidad++;
        }else
        {
            node->updateUtilityDad();
            tree.remove(tree.size()-1);
            cantidad++;
        }
        /*--------------------------------*/
        delete node;
        node = 0;
        /*--------------------------------*/
    }

    //crear la jugada
    Action move = tree.first()->getNext()->getAction();

    int utility = tree.first()->getNext()->getUtility();

    qDebug()<<utility;

    //limpiar tree
    /*--------------------------------*/
    delete rootNode;
    rootNode = 0;
    tree.clear();
    /*--------------------------------*/

    //actualizar el tablero y la posicion del chip
    //se hace en scene

    //cout  << "termina play" << endl;
    cout << "cantidad nodos " << cantidad << endl;
    emit moveResponse(move);
}

bool Game::positionValidate(int x, int y)
{
    if(x >= 0 && x < rowMax && y>= 0 && y<columnMax){
        return true;
    }else {
        return false;
    }
}

int Game::protectedKing(Chip::color color,QPoint position,char** board)
{
    int dx[]={-1,-1,-1,0,0,1,1,1};
    int dy[]={-1,0,1,-1,1,-1,0,1};

    int x=position.x();
    int y=position.y();

    int countProtected = 0;

    for(int i=0; i<8;i++)
    {
        int xNew = x+dx[i];
        int yNew = y+dy[i];

        if(positionValidate(xNew,yNew) && board[xNew][yNew] != '0')
        {
            QChar c(board[xNew][yNew]);
            if(color == Chip::Blanco)
            {
                if( (c.isDigit() && (c.toAscii()%2)==1) || c.isLower() )
                {
                    //                   qDebug()<< board[xNew][yNew];
                    countProtected+=3;
                }
            }else if(color == Chip::Negro)
            {
                if( (c.isDigit() && (c.toAscii()%2)==0) || c.isUpper() )
                {
                    //                     qDebug()<< board[xNew][yNew];
                    countProtected +=3;
                }
            }
        }
    }

    //cout  << "termina protectedKing" << endl;
    return countProtected;
}

void Game::printChips(QVector<Chip> *chips)
{
    for(int i=0;i<chips->size();i++)
    {
        cout << "id: " << (*chips)[i].getId() << " pos: (" << (*chips)[i].getPosition().x() << (*chips)[i].getPosition().y() << ") dir: " << &(*chips)[i] << endl;
    }
}

void Game::printBoard(char **board)
{
    cout << "\n";
    for(int i=0;i<rowMax;i++)
    {
        for(int j=0;j<columnMax;j++)
        {
            cout << board[i][j] << " ";
        }
        cout << endl;
    }
}

void Game::setBoard(char **board)
{
    //FALTA
}

void Game::setChips(QVector<Chip> chips)
{
    //FALTA
}

void Game::setColumnMax(int columnMax)
{
    this->columnMax = columnMax;
}

void Game::setRowMax(int rowMax)
{
    this->rowMax = rowMax;
}

void Game::setDepth(int depth)
{
    this->depth = depth;
}

bool Game::drawGame()
{
    if(chips.size() == 2)
    {
        emit drawGameResponse(true);
        return true;
    }else
    {
        emit drawGameResponse(false);
        return false;
    }
}

void Game::updatedBoard(char**board, QPoint positionBefore, QPoint positionAfter, char id)
{
    int xA = positionBefore.x();
    int yA = positionBefore.y();
    int xB = positionAfter.x();
    int yB = positionAfter.y();

    board[xA][yA] = '0';
    board[xB][yB] = id;
}

int Game::utilityHeuristicFunction(Node* node)
{
    QVector<Chip> chips = node->getChips();
    char** boardFind = node->getBoard();
    Node::type type= node->getType();

    QPoint whiteKingPosition(-1,-1);


    QPoint blackKingPosition(-1,-1);


    findKingPosition(chips,&whiteKingPosition,&blackKingPosition);



    int valorJaque = 0;

    int total = 0;



    // Suma de los valores de las piezas
    int valueWhiteChips = findValueChips(Chip::Blanco,chips);
    int valueBlackChips = findValueChips(Chip::Negro,chips);

    int protectedKingWhite = 0;
    bool whiteKingCheck = false;
    int protectedKingBlack = 0;
    int blackKingCheck= false;




    if(whiteKingPosition != QPoint(-1,-1))

    {
        protectedKingWhite = protectedKing(Chip::Blanco,whiteKingPosition,boardFind);
        whiteKingCheck = inCheck(whiteKingPosition,Chip::Blanco,&chips,true,boardFind);
    }


    if(blackKingPosition != QPoint(-1,-1))

    {
        protectedKingBlack = protectedKing(Chip::Negro,blackKingPosition,boardFind);
        blackKingCheck = inCheck(blackKingPosition,Chip::Negro,&chips,true,boardFind);
    }


    if(whiteKingCheck  && type == Node::Max)
    {
        valorJaque = -200;
    }else if(blackKingCheck && type== Node::Min)
    {
        valorJaque = 200;
    }




    total = (valueWhiteChips-valueBlackChips) + (protectedKingWhite-protectedKingBlack)+valorJaque;


    /*--------------------------------*/
    //el board que retorna node es una copia y no se usara como parametro para atributo de alguna clase
    for(int i=0; i<this->rowMax; i++)
    {
        delete boardFind[i];
        boardFind[i] = 0;
    }
    delete boardFind;
    boardFind= 0;
    /*--------------------------------*/

    //cout  << "termina utilityHuristicFunction" << endl;
    return total;
}
