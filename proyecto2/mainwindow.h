 /*
  *Nombre archivo: mainwindow.h
  *Autor: Cristian Leonardo Ríos López
  *Fecha creación: 20 Septiembre de 2011
  *Fecha última actualización: Enero 2012
  *Descripción: La clase MainWindow es la clase principal, permite reunir todos lo elementos
      gráficos que permiten interactuar con el programa, permite cargar estados de búsqueda,
      como tambien la creación dinámica de estos. Teniendo un estado de búsqueda permite
      ejecutar los algoritmos de busqueda y usando otras clases mostrar los resultado y
      las anmaciones de estos.
  *Universidad del Valle
  */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include "scene.h"
#include "game.h"

QT_BEGIN_NAMESPACE
class QGraphicsView;
class QComboBox;
class QSpinBox;
class QPushButton;
class QToolBox;
class QButtonGroup;
class QWidget;
class QStringList;
class QLabel;
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:

    Game *game;

    void createActions();
    void createMenus();

    QGraphicsView *view;
    Scene *scene;

    QMenu *fileMenu;
    QMenu *helpMenu;
    QMenu *levelMenu;
    QMenu *GameMenu;

    QAction *exitAction;
    QAction *aboutAction;
    QAction *helpAction;
    QAction *beginnerLevelAction;
    QAction *amateurLevelAction;
    QAction *expertLevelAction;
    QAction *newGameAction;

 private slots:
    void about();
    void help();
    void beginnerLevel(bool checked);
    void amateurLevel(bool checked);
    void expertLevel(bool checked);
    void newGame();
    void startGame();

public slots:
    void cursorWait();
    void cursorArrow(Action);
    void verifyInChekMin(QPoint);
};

#endif // MAINWINDOW_H
