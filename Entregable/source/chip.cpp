#include "chip.h"

Chip::Chip(){}

Chip::Chip(QPoint position, color myColor,type myType,char id)
{
    this->position = position;
    this->myColor = myColor;
    this->myType = myType;
    this->id = id;
}

Chip::Chip(Chip *chip)
{
    this->position = chip->getPosition();
    this->myColor = chip->getColor();
    this->myType = chip->getType();
    this->id = chip->getId();
}

void Chip::setPosition(QPoint position)
{
    this->position = position;
}

void Chip::setColor(color c)
{
    this->myColor = c;
}

void Chip::setType(type t)
{
    this->myType = t;
}

void Chip::setId(char id)
{
    this->id = id;
}

QPoint Chip::getPosition()
{
    return position;
}

Chip::color Chip::getColor()
{
    return myColor;
}

Chip::type Chip::getType()
{
    return myType;
}

char Chip::getId()
{
    return id;
}
