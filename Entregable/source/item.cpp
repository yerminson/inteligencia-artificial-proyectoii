/*
*
* Nombre archivo : item.h
* Nombre de la clase : Item
* Autor:
* Autor: Cristian Leonardo Rios
* Fecha de creación: Octubre del 2011
* Fecha de modificación: Diciembre del 2011
*
* Universidad del Valle
*/
#include "item.h"

Item::Item(const QPixmap pix, int width, int height, int x, int y, char letter)
    :QObject(), QGraphicsPixmapItem(pix)
{
    this->width = width;
    this->height = height;
    this->letter = letter;
    this->x = x;
    this->y = y;

    this->setPixmap(this->pixmap().scaled(width, height));
    this->setPos(x,y);

    //setFlag(QGraphicsItem::ItemIsSelectable, true);
}

/*
* Metodo setWidth: Permite cambiar el ancho de un item que esta representado en el canvas.
* Entradas: Un numero que representa el nuevo ancho del item.
* Salida: No hay salida, ya que solo representa un cambio en el atributo width del item.
*/
void Item::setWidth(int width)
{
    this->width = width;
}

/*
* Metodo setHeight: Permite cambiar la altura de un item que esta representado en el canvas.
* Entradas: Un numero que representa el nuevo ancho del item.
* Salida: No hay salida, ya que solo representa un cambio en el atributo height del item.
*/
void Item::setHeight(int height)
{
    this->height = height;
}

/*
* Metodo setX:  Permite cambiar la posicion en x del item.
* Entradas: Un numero que representa el nuevo valor para la posicion en x del item.
* Salida: No hay salida, ya que solo representa un cambio en el atributo x del item.
*/
void Item::setX(int x)
{
    this->x = x;
}

/*
* Metodo setY: Permite cambiar la posicion y del item.
* Entradas: Un numero que representa el nuevo valor para la posicion y del item.
* Salida: No hay salida, ya que solo representa un cambio en el atributo y del item.
*/
void Item::setY(int y)
{
    this->y = y;
}

/*
* Metodo setLetter: Permite cambiar la letra que esta asociada al item.
* Entradas: Un caracter que representa la letra por la cual se va a modificar la letra del item.
* Salida: No hay salida, ya que solo representa un cambio en el atributo letter en la accion.
*/
void Item::setLetter(char letter)
{
    this->letter = letter;
}

/*
* Metodo getWidth: Permite obtener el ancho del item.
* Entradas: No hay entrada, ya que es una consulta.
* Salida: Un numero que representa el ancho del item.
*/
int Item::getWidth()
{
    return width;
}

/*
* Metodo getHeight: Permite obtener la altura del item .
* Entradas: No hay entrada, ya que es una consulta.
* Salida: Un numero que representa la altura del item.
*/
int Item::getHeight()
{
    return height;
}

/*
* Metodo getX:Permite obtener la posicion en x del item.
* Entradas: No hay entrada ya que solo es un metodo de consulta.
* Salida: Un numero representando la coordenada inicial en y.
*/
int Item::getX()
{
    return x;
}

/*
* Metodo getY: Permite obtener la posicion en y del item.
* Entradas: No hay entrada, ya que solo es una consulta.
* Salida: Un numero que representa la coordenada y del item.
*/
int Item::getY()
{
    return y;
}

/*
* Metodo getLetter: Permite obtener la letra con la cual esta representada el item.
* Entradas: No hay entrada, ya que solo es una consulta.
* Salida: Un caracter mostrando la letra que tiene el item.
*/
char Item::getLetter()
{
    return letter;
}
