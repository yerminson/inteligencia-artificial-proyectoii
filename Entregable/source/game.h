/*
 *Nombre archivo: game.h
 *Autores: Maria Andrea Cruz Blandon
           Cristian Leonardo Ríos López
           Yerminson Doney Gonzalez Muñoz

 *Fecha creación: 20 Septiembre de 2011
 *Fecha última actualización: Enero del 2012
 *Descripción: La clase game permite representar la estrutuctura de todo el juego que se
 esta representando en este caso el ajedrez, los tres etapas importantes que se representan
 son la generacion del juego, el desarollo del juego y la funcion de utilidad aplicada para
 que max pueda tomar desiciones.
 *Universidad del Valle
 */
#ifndef GAME_H
#define GAME_H

#include <QVector>
#include <QChar>
#include <QObject>
#include "action.h"
#include "node.h"
#include "chip.h"

class Game : public QObject
{
    Q_OBJECT

public:
    Game(int columnMax, int rowMax, QObject *parent=0);
    ~Game();
    void clearStates();
    char** getBoard();
    QVector<Chip>* getChips();
    void generateBoard();
    int getColumnMax();
    int getRowMax();
    int getDepth();
    bool inCheck(QPoint possiblePosition, Chip::color color,QVector<Chip> *chipsFind = 0, bool heuristic = false, char **boardFind = 0);
    void setBoard(char **board);
    void setChips(QVector<Chip> chips);
    void setColumnMax(int columnMax);
    void setRowMax(int rowMax);
    void setDepth(int depth);
    void play();
    void printChips(QVector<Chip> *chips);

private:
    int amountPawns;
    int amountKnights;
    int amountRooks;
    int amountQueens;
    int amountKings;
    int amountBishops;
    char **board;
    int columnMax;
    int depth;
    QVector<Chip> chips;
    int rowMax;

    Node* applyAction(Node* dad,Action* action);
    void createChips(int amount,Chip::type type,QChar id,QVector<QPoint>* positions);
    int distanceToKing(Chip::color color, QPoint position, QVector<Chip> chips);
    QVector<Action> findActions(Chip *chip, char **boardFind=0);
    Chip* findChip(char id, QVector<Chip> *chipsFind = 0);
    void findKingPosition(QVector<Chip> chips, QPoint* whiteKingPosition, QPoint* blackKingPosition);
    int findValueChips(Chip::color color, QVector<Chip> chips);
    char** getCopyBoard();
    bool inCheckEndMax(QVector<Node*> *tree);
    bool isAttack(char id, Chip::color);
    bool positionValidate(int x, int y);
    int protectedKing(Chip::color color,QPoint position,char** board);
    void printBoard(char **board);
    int utilityHeuristicFunction(Node* node);
    void updatedBoard(char**board, QPoint positionBefore, QPoint positionAfter, char id);

signals:
    void moveResponse(Action);
    void possibleMoves(QVector<QPoint>);
    void inCheckEndMinResponse(bool);
    void inCheckMinResponse(bool);

public slots:
    void getPossibleMoves(char id);
    bool inCheckEndMin();
    bool inCheckMin(Action);
};

#endif // GAME_H
