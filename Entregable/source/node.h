/*
 *Nombre archivo: node.h
 *Autor: Cristian Leonardo Ríos López
 *Fecha creación:  Septiembre de 2011
 *Fecha última actualización: Enero del 2012
 *Descripción: La clase Node permite representar un nodo es decir la representacion de un estado del juego
 dentro del arbol que se constituye para realizar el algoritmo minimax , los atributos que se tienen en el
 nodo son el tablero que representa como va el juego, las fichas que estan dentro del juego, la utilidad que
 tiene este nodo para max, el tipo de nodo si es min o max, un puntero al padre, un puntero al nodo que le
 otorgo la mejor utilidad, el ancho , el alto del tablero, la profundidad  y la accion con la que se llego a ese nodo.
 *Universidad del Valle
 */

#ifndef NODE_H
#define NODE_H

#include "chip.h"
#include "action.h"
#include <QVector>

class Node
{
public:
    enum type {Max, Min};
private:
    char** board;
    QVector<Chip> chips;
    int utility;
    type myType; //tipo de nodo 1 min, 0 max
    Node* dad;
    Node* next;
    int width;//tamano del tablero
    int height;
    int depth;
    Action action;//accion con la que se llego al nodo
public:
    Node(Node* dad,char** board, int width, int height,QVector<Chip> chips,type myType,int depth, Action action);
    ~Node();

    void setBoard(char** board);
    void setChips(QVector<Chip> chips);
    void setUtility(int utility);
    void setType(type myType);
    void setWidth(int width);
    void setHeight(int height);
    void setDad(Node* dad);
    void setDepth(int depth);
    void setNext(Node *nextNode);
    void setAction(Action action);

    char** getBoard();
    QVector<Chip> getChips();
    int getUtility();
    type getType();
    int getWidth();
    int getHeight();
    Node* getDad();
    int getDepth();
    Node* getNext();
    Action getAction();

    void updateUtility(int utility, Node* who);
    void updateUtilityDad();
};

#endif // NODE_H
