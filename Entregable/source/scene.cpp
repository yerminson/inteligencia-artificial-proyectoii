/*
  *Nombre archivo: scene.cpp
  *Autor: Cristian Leonardo Ríos López
  *Fecha de creación: Septiembre 2011
  *Fecha de ultima modificación: Diciembre 2011
  *Universidad del Valle
*/

#include "scene.h"
#include <iostream>
#include <cmath>
#include <iostream>
#include <QDebug>
#include <QGraphicsLineItem>
#include <QList>
#include <QGraphicsSceneMouseEvent>
#include <QMessageBox>
#include <QPropertyAnimation>
#include <ctime>

using namespace std;

Scene::Scene(int rowsMax, int columnsMax, QObject *parent)
 : QGraphicsScene(parent)
{
    this->columnMax= columnsMax;
    this->rowMax= rowsMax;
    chipGame = 0;
    chips = 0;
    board = 0;
    freeBlack = 0;
    freeWhite = 5;

    animationGroup = new QSequentialAnimationGroup;
    connect(animationGroup,SIGNAL(finished()),this,SLOT(animationFinished()));

    gameHuman = false;
    isCheckEndMin = false;
    isCheckMin = false;
    isCheckEndMax = false;

    this->setBackgroundBrush(QBrush(QColor(255,250,240)));
}

Scene::~Scene()
{
    // board //no pertenece a scene, no le corresponde tratarlo
    if(board != 0)
    {
        for(int i=0; i<this->rowMax; i++)
        {
            board[i]= 0;
        }
        board = 0;
    }
    delete animationGroup;
    animationGroup = 0;

    //chipGame // hacereferencia a un objeto que no le pertenece, no le corresponde tratarlo
    chipGame = 0;
}

void Scene::animationFinished()
{
    //Como inicialmente juega la maquina game human es false, por lo tanto
    //despues de realizar la primera jugada es que se tiene en cuenta cuando termina de jugar el humano
    // para que juegue la maquina
    //gameHuman = !gameHuman;

    animationGroup->clear();

    //se emite una senal indicando que la jugda a sido realizada
    if(!gameHuman)
    {
        emit moveMade();
    }
}

/*
  *Metodo clearBoard: limpia todos los elementos del tablero. el tablero queda vacio.
  *Entrada: No tiene parametros de entrada.
  *Salida: La modificacion de los vectores que contiene los graficos de los carros y los obstaculos
  *        dejandolos vacios.
*/
void Scene::clearBoard()
{

}

void Scene::clearPossibleMoves()
{
    possibleMoves.clear();
}

/*
  *Metodo drawGrid: Dibuja el tablero, lineas horizontales y verticales, y hace el llamado a drawElements.
  *Entrada: No tiene parametros de entrada.
  *Salida: La modificacion del QGraphicsScene mostrando las lineas del tablero y los elementos.
*/
void Scene::drawGrid()
{
    /*limpiar el tablero*/
    //this->clear(); //No se puede usar clear, este destruye la referencia a los objetos en el vector de graphics
    QList<QGraphicsItem*> elements = items();
    foreach(QGraphicsItem* i,elements)
    {
        removeItem(i);
    }

    QGraphicsLineItem *lineV1 = new QGraphicsLineItem(QLineF(QPointF(-10,-10),QPointF(-10,height()+10)));
    QGraphicsLineItem *lineV2 = new QGraphicsLineItem(QLineF(QPointF(width()+10,-10),QPointF(width()+10,height()+10)));
    QGraphicsLineItem *lineH1 = new QGraphicsLineItem(QLineF(QPointF(-10,-10),QPointF(width()+10,-10)));
    QGraphicsLineItem *lineH2 = new QGraphicsLineItem(QLineF(QPointF(-10,height()+10),QPointF(width()+10,height()+10)));

    QPen pen;
    pen.setWidth(7);
    pen.setJoinStyle(Qt::RoundJoin);
    lineV1->setPen(pen);
    lineV2->setPen(pen);
    lineH1->setPen(pen);
    lineH2->setPen(pen);
    addItem(lineV1);
    addItem(lineV2);
    addItem(lineH1);
    addItem(lineH2);

    drawBoard();
    drawElements();
}

void Scene::drawBoard()
{
    int dx = floor(width()/columnMax);
    int dy = floor(height()/rowMax);

    bool cualCuadro = false; //false es negro

    for(int i=0;i<rowMax;i++)//cantidad filas
    {
        for(int j=0;j<columnMax;j++)//cantidad columnas
        {
            if(cualCuadro)
            {
                QPixmap pix(":/images/blanco.jpg");
                QGraphicsPixmapItem *cuadro = new QGraphicsPixmapItem();
                cuadro->setPos(dx*j,dy*i);
                cuadro->setPixmap(pix.scaled(dx, dy));
                this->addItem(cuadro);
                cualCuadro = false;
            }else
            {
                QPixmap pix(":/images/negro.jpg");
                QGraphicsPixmapItem *cuadro = new QGraphicsPixmapItem();
                cuadro->setPos(dx*j,dy*i);
                cuadro->setPixmap(pix.scaled(dx, dy));
                this->addItem(cuadro);
                cualCuadro = true;
            }
        }
        cualCuadro = !cualCuadro;
    }
}

/*
  *Metodo drawElements: Dibuja los carros y obstaculos en el tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: Modifica el QGraphicsScene dibujando los carros y obstaculos.
*/
void Scene::drawElements()
{
    foreach(Item *item,chipsGraphics)
    {
        this->addItem(item);
    }
}

void Scene::drawSelectChip(QPoint position)
{
    int dx = floor(width()/columnMax);
    int dy = floor(height()/rowMax);

    /*----------------*/
    selected = new QGraphicsEllipseItem(0,0,dx,dy);
    selected->setPos(QPointF(position.y()*dx,position.x()*dy));

    QPen pen;
    pen.setWidth(4);
    pen.setStyle(Qt::DashDotLine);
    pen.setColor(Qt::red);
    selected->setPen(pen);

    this->addItem(selected);
}

void Scene::drawSelectPossibleMove()
{
    int dx = floor(width()/columnMax);
    int dy = floor(height()/rowMax);

    for(int i=0;i<possibleMoves.size();i++)
    {
        QPoint point = possibleMoves.at(i);
        if(this->board[point.x()][point.y()] != 'k')
        {
            QPoint pos(point.y()*dx,point.x()*dy);
            selectPossibleMoves.append(new QGraphicsRectItem(QRect(pos,QSize(dx,dy))));
        }else{
            possibleMoves.remove(i);
            i--;
        }
    }

    QPen pen;
    pen.setWidth(3);
    pen.setStyle(Qt::DashDotLine);
    pen.setColor(Qt::blue);

    foreach(QGraphicsRectItem *r,selectPossibleMoves)
    {
        r->setPen(pen);
        this->addItem(r);
    }
}

void Scene::eraseSelectPossibleMove()
{
    foreach(QGraphicsRectItem *r,selectPossibleMoves)
    {
        this->removeItem(r);
        delete r;
        r=0;
    }
    selectPossibleMoves.clear();
}

Item* Scene::findChip(char id)
{
    Item* chip = 0;
    for(int i=0;i<chipsGraphics.size();i++)
    {
        if(chipsGraphics.at(i)->getLetter() == id)
        {
            chip = chipsGraphics.at(i);
            break;
        }
    }

    return chip;
}

/*posicion se refiere a la que ocupa en el tablero, el id es usado para discriminar la busqueda
puesto que en algun momento pueden existir dos chips con la misma posicion, cosa que no ocurre
con los identificadores que son unicos*/
Chip* Scene::findChip(QPoint position, char id)
{
    Chip *chip = 0;
    if(id != 'z')
    {
        for(int i=0;i<chips->size();i++)
        {
            if((*chips)[i].getId() == id)
            {
                chip = &(*chips)[i];
                break;
            }
        }
    }else
    {
        for(int i=0;i<chips->size();i++)
        {
            if((*chips)[i].getPosition() == position)
            {
                chip = &(*chips)[i];
                break;
            }
        }
    }

    return chip;
}

QPoint Scene::findFreePosition(char id)
{
    Chip::color color;
    QChar idq = QChar(id);
    if((id > 48) && (id < 57)) //numero entre 1 y 8
    {
        if(id%2!=0)
        {
            color = Chip::Blanco;
        }else //es par
        {
            color = Chip::Negro;
        }

    }else//es letra
    {
        if(idq.isLower())
        {
            color = Chip::Blanco;
        }else //is upper
        {
            color = Chip::Negro;
        }

    }

    if(color == Chip::Blanco)//maquina
    {
        return QPoint(freeWhite--,-1);
    }else//humano
    {
        return QPoint(freeBlack++,6);
    }
}

/*
  *Metodo getBoard: retorna una copia del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: Un arreglo que representa el tablero.
*/
char** Scene::getBoard()
{
    return board;
}

/*
  *Metodo getRowMax: retorna el numero de filas del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: El entero que corresponde a las filas del tablero
*/
int Scene::getRowMax()
{
    return rowMax;
}

/*
  *Metodo getColumnMax: retorna el valor de las columnas del tablero.
  *Entrada: No tiene parametros de entrada.
  *Salida: El enterio que corresponde al valor de las columnas del tablero.
*/
int Scene::getColumnMax()
{
    return columnMax;
}

/*
  *Metodo howPositionBoard: retorna la posicion en el tablero(archivo entrada) dada una posicion en el grafico del tablero.
  *Entrada: point representa la posicion de un elemento en el tablero grafico.
  *Salida: Qpoint que representa la posicion en el tablero(archivo entrada).
*/
QPoint Scene::howPositionBoard(QPointF point)
{
    int dx = floor(width()/columnMax);
    int dy = floor(height()/rowMax);

    QPoint position(-1,-1);

    for(int i=0;i<columnMax;i++)
    {
        if((point.x() >= dx*i) && (point.x() < dx*(i+1)))
        {
            for(int j=0;j<rowMax;j++)
            {
                if((point.y() >= dy*j) && (point.y() < dy*(j+1)))
                {
                    position.setX(j);
                    position.setY(i);
                }
            }
        }
    }

    return position;
}

//slot
void Scene::inCheckMin(bool inCheck)
{
    this->isCheckMin = inCheck;
}

//slot
void Scene::inCheckEndMin(bool inCheckEnd)
{
    this->isCheckEndMin = inCheckEnd;
    if(inCheckEnd)
    {
        this->mensaje(tr("Usted a perdido, se encuentra en Jaque Mate."));
    }
}

void Scene::mensaje(QString msj)
{
    QMessageBox::about(0,tr("Jugada"),msj);
}

/*
  *Metodo mousePressEvent: recibe un evento del mouse si este es un clic derecho, se hace en una posicion valida del tablero
  *                        y no interfiere con otro elemento entonces se inserta el elemento que se halla elegido en la interfaz.
  *Entrada: mouseEvent representa el clic del mouse hecho en el tablero.
  *Salida: el ingreso del elemento(grafico) en el tablero, por supuesto si es valida la operacion.
*/
void Scene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if(isCheckEndMin)
    {
        this->mensaje(tr("Usted a perdido, se encuentra en Jaque Mate."));
        return;
    }
    if(isCheckEndMax)
    {
        this->mensaje(tr("Ud ha Ganado, has hecho Jaque Mate."));
        return;
    }

    if(!gameHuman)
            return;

    if(board == 0)
        return;

    QPoint position = howPositionBoard(mouseEvent->scenePos());

    if(!validatePosition(position))
        return;

    //si chipGame es 0 se va a iniciar un ajugada
    if(chipGame == 0)
    {
        //si en la posicion en el tablero hay cero, no hay ficha para iniciar la jugada
        if(board[position.x()][position.y()] == '0')
            return;

        chipGame = findChip(position);

        //se verifica si se selecciona una ficha blanca, con esas no se puede jugar el humano
        if(chipGame->getColor() == Chip::Blanco)
        {
            chipGame = 0;
            return;
        }

        //muestra la seleccion de la ficha con la que se va a jugar
        this->drawSelectChip(position);

        //se emite una senal con el id del chip del cual se quiere validar la jugada
        //esto en realidad hara que se actualice el vector possibleMoves con el cual
        //se validara la jugada
        emit getPossibleMoves(chipGame->getId());
        this->drawSelectPossibleMove();

    }else//se a seleccionado una pieza y se va a realizar la segunda parte de la jugada
    {
        //se a vuelto a dar click en la pieza con la que se pretendia jugar
        if(chipGame->getPosition() == position)
        {
            this->removeItem(selected);
            this->eraseSelectPossibleMove();
            this->clearPossibleMoves();
            chipGame = 0;
            return;
        }

        //se valida si la jugada que se trata de hacer es valida
        bool valid = moveValid(position);
        if(!valid)
        {
            this->mensaje(tr("La jugada que intenta realizar nos es válida, por favor intentelo nuevamente."));
            return;
        }

        //probar si la jugada que trato de hacer me deja en jaque
            //se crea el action que representa la posible jugada
        Action possibleAction(chipGame->getId(),position);
        emit verifyInCheckMin(possibleAction);
        if(this->isCheckMin)
        {
            this->mensaje(tr("La jugada que intenta realizar te dejaria en Jaque, por favor intentelo nuevamente."));
            return;
        }

        //si la jugada es valida quitamos la seleccion a la ficha con la que se jugo
        this->removeItem(selected);
        this->eraseSelectPossibleMove();
        this->clearPossibleMoves(); //se limpia el vector de posibles jugadas

        QPoint positionBefore = chipGame->getPosition();

        //se crea el vector que contendra las accion de la jugada
        QVector<Action> moves;

        //se agrega la jugada, se sabe que es posible
        moves.append(possibleAction);

        //se actualiza la posicion de la pieza con la que se jugo
        chipGame->setPosition(position);

        char id = chipGame->getId();

        //se verifica si la jugada es una ataque
        this->moveIsAttack(position, &moves);

        //se actualiza el tablaro con la jugada
        updateBoard(positionBefore, position, id);

        //ya no hay pieza seleccionada para jugada, queda para realizar una nueva jugda
        chipGame = 0;

        gameHuman=false;
        //se corre la animacion
        this->runAnimation(moves);
    }

    QGraphicsScene::mousePressEvent(mouseEvent);
}

void Scene::moveIsAttack(QPoint position, QVector<Action> *moves)
{
    char id = moves->first().getId();

    //si la jugada ralizada come una pieza se crea la jugada y se corre la animacion
    if(board[position.x()][position.y()] != '0')
    {
        Chip *chip;
        int index;
        //se busca la pieza que fue comida y se corre la animacion
        for(int i=0;i<chips->size();i++)
        {
            //como existe una pieza que tiene la misma posicion que la que busco, miro que sus id sean distintos
            if(((*chips)[i].getPosition() == position) && ((*chips)[i].getId() != id))
            {
                chip = &(*chips)[i];
                index = i;
                break;
            }
        }

        QPoint freePosition = findFreePosition(chip->getId());
        Action b(chip->getId(),freePosition);
        moves->append(b);

        //se quita la pieza que se movio y salio del tablero
        chips->remove(index);
        chip = 0;
    }
}

void Scene::moveMadeResponse(Action action)
{
    if(action.getId() == 'J')
    {
        this->isCheckEndMax = true;
        this->mensaje(tr("Ud ha Ganado, has hecho Jaque Mate."));
        return;
    }

    //se actualiza la posicion de la pieza con la que se juega
    Chip *chip = findChip(QPoint(),action.getId());
    char id = action.getId();
    QPoint position = action.getPosition();
    QPoint positionBefore = chip->getPosition();
    chip->setPosition(position);

    // se crea el vector que tendra las accion para ser animadas
    QVector<Action> moves;
    moves.append(action);

    this->moveIsAttack(position,&moves);

    //se actualiza el tablero
    updateBoard(positionBefore, position, id);

    chip = 0;
    this->runAnimation(moves);
    gameHuman=true;
    emit verifyInCheckEndMin();
}

bool Scene::moveValid(QPoint position)
{
    bool valid = false;
    foreach(QPoint i,possibleMoves)
    {
        if(i == position)
        {
            valid = true;
            break;
        }
    }
    return valid;
}

void Scene::newGame(char** board,QVector<Chip> *chips)
{
    this->chipGame = 0;
    this->chips = 0;//no pertenecen a scene
    this->board = 0;//no pertenece a scene
    this->freeBlack = 0;
    this->freeWhite = 5;
    gameHuman = false;
    isCheckEndMin = false;
    isCheckMin = false;
    isCheckEndMax = false;

    if(chipsGraphics.size() != 0)
    {
        foreach(Item *c,chipsGraphics)
        {
            delete c;
            c=0;
        }
    }
    chipsGraphics.clear();
    setBoard(board);
    setChips(chips);
}

void Scene::printBoard()
{
    cout << "\n";
    for(int i=0;i<rowMax;i++)
    {
        for(int j=0;j<columnMax;j++)
        {
           cout << board[i][j] << " ";
        }
        cout << endl;
    }  
}

/*
  *Metodo runAnimation: muestra como los carros se mueven apartir de un vector de acciones (movimientos).
  *Entrada: movements vector de action que represeta los pasos para irse moviendo y llegar a la meta.
  *Salida: la animacion en el QGraphicsScene de los elementos(carros).
*/
void Scene::runAnimation(QVector<Action> moves)
{
   int dx = floor(width()/columnMax);
   int dy = floor(height()/rowMax);

   for(int i=0;i<moves.size();i++)
   {
       Action move = moves.at(i);
       char id = move.getId();
       Item* chip = findChip(id);

       QPropertyAnimation* animation = new QPropertyAnimation(chip,"pos");
       animation->setDuration(1000);
       animation->setStartValue(QPoint(chip->getX(),chip->getY()));
       animation->setEasingCurve(QEasingCurve::InOutBack);
       int xEnd = move.getPosition().y()*dx;
       int yEnd = move.getPosition().x()*dy;
       animation->setEndValue(QPoint(xEnd,yEnd));
       animationGroup->addAnimation(animation);
       chip->setX(xEnd);
       chip->setY(yEnd);
       chip=0;
   }
   animationGroup->start();
}

/*
  *Metod setBoard: cambia el tablero con la informacion del tablero de entrada.
  *Entradas: board representa un tablero, rows representa las filas del tablero y columns las columnas del tablero.
  *Salida: la modificacion del tablero con la nueva información.
*/
void Scene::setBoard(char** board)
{
    this->board = board;
}

/*
  *Metodo setColumnMax: Cambia el valor de columnMax por un nuevo valor.
  *Entrada: columnMax entero que representa el numero de columnas del tablero.
  *Salida: La modificacion del atributo columnMax con el nuevo valor.
*/
void Scene::setColumnMax(int columnMax)
{
    this->columnMax = columnMax;
}

void Scene::setChips(QVector<Chip> *chips)
{
    int dx = floor(width()/columnMax);
    int dy = floor(height()/rowMax);

    //this->chipsGraphics.clear();
    this->chips = chips;

    foreach(Chip chip,*chips)
    {
        int x = chip.getPosition().x();
        int y = chip.getPosition().y();
        char id = chip.getId();
        QPixmap pix(whichImage(&chip));
        Item *item = new Item(pix,dx,dy,dx*y,dy*x,id);
        chipsGraphics.append(item);
    }
}

void Scene::setPossibleMoves(QVector<QPoint> moves)
{
    possibleMoves = moves;
}

/*
  *Metodo setRowMax: cambia el valor del atributo rowMax por un nuevo valor.
  *Entrada: rowMax entero que representa el nuevo valor de filas del tablero.
  *Salida: la modificacion del atributo rowMax.
*/
void Scene::setRowMax(int rowMax)
{
    this->rowMax = rowMax;
}

/*
  *Metodo updateBoard: Actualiza los valores que tiene board en cada una de sus celdas
  *Entradas: posistion que representa la posicion a actualizar, type que representa el tipo del elemento, action representa si
  *          se va a eliminar '0' o agregar elemento '1' y letter representa el id del carro por si el elemento es un carro.
  *Salida: la modificcion del atributo board con la nueva información.
*/
void Scene::updateBoard(QPoint positionBefore,QPoint positionAfter, char letter)
{
    int xB = positionBefore.x();
    int yB = positionBefore.y();
    int xA = positionAfter.x();
    int yA = positionAfter.y();

    board[xB][yB] = '0';
    board[xA][yA] = letter;
}


/*
  *Metodo validatePosition: verifica si una posicion en el tablero (dada con el mouse) es valida para ingresar elementos.
  *Entrada: position que representa el punto donde se dio clic.
  *Salida: el booleano que responde con true si la posicion es valida en caso contrario false.
*/
bool Scene::validatePosition(QPoint position)
{
    bool validate = false;

    int x = position.x();
    int y = position.y();

    if((x != -1) && (y != -1))
    {
        validate = true;
    }

    return validate;
}

/*
  *Metodo whichImage: verifica que imagen se debe usar para determinado carro.
  *Entrada: direction representa la direccion del carro, letter el id del carro y size el tamaño del carro.
  *Salida: la string con el nombre de la imagen a usar.
*/
QString Scene::whichImage(Chip *chip)
{
    Chip::color color = chip->getColor();
    Chip::type type = chip->getType();
    QString image;

    switch(color)
    {
        case Chip::Blanco:
            switch(type)
            {
                case Chip::Peon:
                    image = ":/images/peonBlanco.png";
                    break;
                case Chip::Alfil:
                    image = ":/images/alfilBlanco.png";
                    break;
                case Chip::Caballo:
                    image = ":/images/caballoBlanco.png";
                    break;
                case Chip::Reina:
                    image = ":/images/damaBlanco.png";
                    break;
                case Chip::Rey:
                    image = ":/images/reyBlanco.png";
                    break;
                case Chip::Torre:
                    image = ":/images/torreBlanco.png";
            }
            break;
        case Chip::Negro:
            switch(type)
            {
            case Chip::Peon:
                image = ":/images/peonNegro.png";
                break;
            case Chip::Alfil:
                image = ":/images/alfilNegro.png";
                break;
            case Chip::Caballo:
                image = ":/images/caballoNegro.png";
                break;
            case Chip::Reina:
                image = ":/images/damaNegro.png";
                break;
            case Chip::Rey:
                image = ":/images/reyNegro.png";
                break;
            case Chip::Torre:
                image = ":/images/torreNegro.png";

            }
    }

    return image;
}
