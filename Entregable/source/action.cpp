#include "action.h"

Action::Action()
{
}

Action::Action(char id, QPoint position)
{
    this->id = id;
    this->position = position;
}

void Action::setId(char id)
{
    this->id = id;
}

void Action::setPosition(QPoint position)
{
    this->position = position;
}

char Action::getId()
{
    return id;
}

QPoint Action::getPosition()
{
    return position;
}
QString Action::toString()
{

    QString output;

    output.append("Id: "+QString(id)+" Position : ("+QString::number(position.x())+","+QString::number(position.y()) +")");

    return output;
}
